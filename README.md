# XiVO-Proxy


## Function :

This proxy runs under nginx.

It is configured in bridge mode in docker and listen only on the xivohost IP. It transmits requests to historical components that are not under docker via the loopback IP (127.0.0.1) accessible thanks to its configuration in bridge mode.


## Proxied components :

- confd (port 9487)
- sysconfd (port 8668)
- provd (port 8666)


## Docker environment variable :

- XIVO_HOST (used for binding nginx on this IP)
